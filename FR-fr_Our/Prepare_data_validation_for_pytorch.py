
import numpy as np
import pandas as pd
import os 
from concurrent.futures import ProcessPoolExecutor
import argparse
from multiprocessing import cpu_count
import shutil
from sklearn.model_selection import train_test_split
from datetime import datetime

_format = '%Y-%m-%d %H:%M:%S.%f' 


def build_from_path(input_dirs, n_jobs=12, tqdm=lambda x: x):

    
    _in = str(os.getcwd())
    print(_in)
    
    try:
        # Change the current working Directory
        os.chdir(_in)
        print("Directory changed")
    except OSError:
        print("Can't change the Current Working Directory")

    for filename in os.listdir(_in):
        if filename.endswith(".csv"):
            print("file found in : " + os.path.join(_in, filename))
            InputFileDirectory = os.path.join(_in, filename)
            dataset = pd.read_csv(InputFileDirectory,sep='|')
            dataset.iloc[:, 0] =   "FR-fr_Our/_wav_utt_22050/" + dataset.iloc[:, 0].values
            X = dataset.iloc[:, 0].values # Here first : means fetch all rows :-1 means except last column
            #dataset = "FR-fr_Our/_wav_utt_22050/"+dataset.iloc[:, 0].values
            Y = dataset.iloc[:, :].values # : is fetch all rows 3 means 3rd column

    # random_state below is a metric that is used by the function to shuffle datas while splitting. If you change the random_state
    # then your split may not be same as previous
    X_train, X_valid, Y_train, Y_valid = train_test_split(X,Y, test_size=0.05, random_state = 2) # 0.05 valid_size means 20%

    # #create filename with date and shape for train 
    # filename_X_train = "X_train_Shape_"+str(X_train.shape[0])+ "_"+datetime.now().strftime(_format)[:-5] 
    # X_train_pd = pd.Series(X_train)
    # X_train_pd.to_csv(filename_X_train, sep = '|', index = False, header = False )

    # #create filename with date and shape for Valid 
    # filename_X_valid = "X_valid_Shape_"+str(X_valid.shape[0])+ "_"+datetime.now().strftime(_format)[:-5] 
    # X_valid_pd = pd.Series(X_valid)
    # X_valid_pd.to_csv(filename_X_valid,sep = '|', index = False, header = False )


    #create filename with date and shape for train 
    filename_Y_train = "Y_train_Shape_"+str(Y_train.shape[0])+ "_"+datetime.now().strftime(_format)[:-16] 
    Y_train_pd = pd.DataFrame(Y_train)
    Y_train_pd.to_csv(filename_Y_train, sep = '|', index = False, header = False )

    #create filename with date and shape for Valid 
    filename_Y_valid = "Y_valid_Shape_"+str(Y_valid.shape[0])+ "_"+datetime.now().strftime(_format)[:-16] 
    Y_valid_pd = pd.DataFrame(Y_valid)
    Y_valid_pd.to_csv(filename_Y_valid,sep = '|', index = False, header = False )
    

    print(Y_train.shape)
    print(Y_valid.shape)





def main():
    print('initializing preprocessing..')
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', default='',type=str)
    args = parser.parse_args()
    input_dirs = args
    build_from_path(input_dirs)


if __name__ == '__main__':
    main()
