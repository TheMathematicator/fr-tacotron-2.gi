#!/bin/bash

source /applis/environments/cuda_env.sh dahu 10.1
source /applis/environments/conda.sh
source ../NVIDIA_official/bin/activate

cd /bettik/PROJECTS/pr-esyn/tacotron2_gpu2/waveglow/
python3 train.py -c config.json

exit 0
