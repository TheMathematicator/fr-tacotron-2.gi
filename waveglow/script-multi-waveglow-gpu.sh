#!/bin/bash

source /applis/environments/cuda_env.sh dahu 10.1
#source /applis/environments/conda.sh
#source ../NVIDIA_official/bin/activate
conda activate condaPy3.6

cd /bettik/PROJECTS/pr-esyn/tacotron2_gpu2/waveglow/
python3 distributed.py -c config.json

exit 0
